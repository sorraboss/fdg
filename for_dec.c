/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   for_dec.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ichubare <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/02 17:58:53 by ichubare          #+#    #+#             */
/*   Updated: 2017/02/07 16:18:39 by ichubare         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

char	*with_min(t_spec_list *list, char *with_width, char *num, int len)
{
	if (list->flag[2] != 0 || list->flag[1] != 0)
		len++;
	if (list->width > len)
	{
		with_width = ft_strnew((size_t)list->width - len);
		with_width = ft_memset(with_width, ' ', (size_t)list->width - len);
		with_width = ft_strjoin(num, with_width);
		return (with_width);
	}
	return (num);
}

char	*without_min(t_spec_list *list, char *with_width, char *num, int len)
{
	if (list->flag[2] == 0 && list->width > len)
	{
		with_width = ft_strnew((size_t)list->width - len);
		if (list->precission <= 0 && list->flag[4] == 1)
			with_width = ft_memset(with_width, '0', (size_t)list->width - len);
		else
			with_width = ft_memset(with_width, ' ', (size_t)list->width - len);
		if ((num[0] == '+' || num[0] == '-') && list->flag[4] == 1 && list->precission == 0)
			return (ft_strjoin(ft_strjoin(ft_strsub(num, 0, 1), with_width),
						ft_strsub(num, 1, (size_t)len - 1)));
		else
			return (ft_strjoin(with_width, num));
	}
	else if (list->width > len)
	{
		with_width = ft_strnew((size_t)list->width - (len + 1));
		if (list->precission <= 0 && list->flag[4] == 1)
			with_width = ft_memset(with_width, '0', (size_t)list->width - (len + 1));
		else
			with_width = ft_memset(with_width, ' ', (size_t)list->width - (len + 1));
		if (((num[0] == '+' || num[0] == '-') && list->flag[4] == 1) && list->precission == 0)
			return (ft_strjoin(ft_strjoin(ft_strsub(num, 0, 1), with_width),
						ft_strsub(num, 1, (size_t)len - 1)));
		else
			return (ft_strjoin(with_width, num));
	}
	return (num);
}

char	*act_width(t_spec_list *list, char *num, intmax_t dec)
{
	char	*with_width;
	int		len;

	len = (int)ft_strlen(num);
	if (((list->type == 'x' || list->type == 'X') && list->flag[4] == 1 && list->flag[3] == 1)
			|| (list->if_p == 1 && list->precission == 0 && list->flag[0] == 1))
		list->width -= 2;
	else if (list->type == 'o' && list->flag[0] == 1 && list->flag[3] == 1)
		list->width--;
	with_width = NULL;
	if (list->flag[1] == 1 && list->flag[0] != 1 && dec >= 0)
		len++;
	if (dec < 0)
	{
		list->flag[2] = 0;
		list->flag[1] = 0;
	}
	if (list->flag[0] == 1)
		with_width = with_min(list, with_width, num, len);
	else
		with_width = without_min(list, with_width, num, len);
	return (with_width);
}

void	*dec_ret(t_spec_list *list, uintmax_t arg, int base)
{
	uintmax_t	dec;
	char		*num;

	if (list->type != 'd' && list->type != 'i')
		list->flag[2] = 0;
	if (list->parameter != 0 && (list->length == 4 || list->length == 3
								 || list->length == 6 || list->length == 7))
		dec = (unsigned int)arg;
	else
		dec = arg;
	if (list->width < 0)
	{
		list->flag[0] = 1;
		list->width *= -1;
	}
	if (dec == 0 && ((((list->type == 'o' || list->type == 'O') && list->if_prec == 1 && list->flag[3] == 0)) ||
					 (list->if_prec == 1 && list->type != 'o')))
		num = "";
	else if ((list->type == 'x' || list->type == 'X' || list->type == 'o') && list->length == 6)
		num = ft_itoa_base_hex(dec, base);
	else if ((list->type == 'U' && list->length == 2) || list->type == 'U')
		num = ft_itoa_base_hex((unsigned long long)dec, base);
	else if ((list->length == 3 || list->length == 7 || (list->length == 1 && list->type == 'u')) && dec != 0)
		num = ft_itoa_base_hex(dec, base);
	else
		num = ft_itoa_base((long long)dec, base);
	if (list->flag[1] == 1)
		list->flag[2] = 0;
	if (list->precission != 0)
	{
		list->flag[2] = 0;
		num = num_precis(list->precission, num);
	}
	if (list->precission <= 0 && list->if_prec == 1)
		list->flag[4] = 0;
	if (list->width != 0)
		num = act_width(list, num, dec);
	if (list->flag[1] == 1 && (long long)dec >= 0)
		num = with_plus(num, dec);
	if (list->flag[2] != 0 && num[0] != '-')
		num = ft_strjoin(" ", num);
	list->n_len = ft_strlen(num);
	return (num);
}


void	*dec_ret1(t_spec_list *list, intmax_t arg, int base)
{
	intmax_t	dec;
	char		*num;

	dec = arg;
	if (list->width < 0)
	{
		list->flag[0] = 1;
		list->width *= -1;
	}
	if (dec == 0 && list->if_prec == 1)
		num = "";
	else
		num = ft_itoa_base((long long)dec, base);
	if (list->flag[1] == 1)
		list->flag[2] = 0;
	if (list->precission != 0)
	{
		list->flag[2] = 0;
		num = num_precis(list->precission, num);
	}
	if (list->precission <= 0 && list->if_prec == 1)
		list->flag[4] = 0;
	if (list->width != 0)
		num = act_width(list, num, dec);
	if (list->flag[1] == 1 && dec >= 0)
		num = with_plus(num, dec);
	if (list->flag[2] != 0 && num[0] != '-')
		num = ft_strjoin(" ", num);
	list->n_len = ft_strlen(num);
	return (num);
}
